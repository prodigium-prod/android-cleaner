import React, { useState, useLayoutEffect, useCallback, useEffect } from 'react';
import { Text, View } from 'react-native';
import { GiftedChat, Avatar } from 'react-native-gifted-chat';
import { collection, addDoc, orderBy, query, onSnapshot } from 'firebase/firestore';
import { database } from '../../config/index';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Chat = () => {
  const [order_id, setOrder_id] = useState(null);
  const [user_id, setUserId] = useState(null);
  const [name, setName] = useState(null);
  const [phone, setPhone] = useState(null);
  const [count, setCount] = useState(0);
  const [messages, setMessages] = useState([]);
  const [newMessage, setNewMessage] = useState('');
  const [userAvatar, setUserAvatar] = useState(null);

  useEffect(() => {
    const fetchAsyncStorageData = async () => {
      const orderChat = await AsyncStorage.getItem('orderChat');
      const userName = await AsyncStorage.getItem('name');
      const userId = await AsyncStorage.getItem('id');
      const userPhone = await AsyncStorage.getItem('phone');

      setOrder_id(orderChat);
      setName(userName);
      setUserId(userId);
      setPhone(userPhone);
    };
    
    fetchAsyncStorageData();
  }, []);

  useLayoutEffect(() => {
    const collectionRef = collection(database, `kilapin-${order_id}`);
    const q = query(collectionRef, orderBy('createdAt', 'desc'));

    const unsubscribe = onSnapshot(q, querySnapshot => {
      setMessages(
        querySnapshot.docs.map(doc => ({
          order_id: doc.data().order_id,
          _id: doc.data()._id,
          createdAt: doc.data().createdAt.toDate(),
          user: doc.data().user,
          text: doc.data().text
        }))
      );
    });

    return unsubscribe;
  }, [order_id, count]);

  const reloadUseLayoutEffect = () => {
    setTimeout(() => {
      setCount(count + 1);
    }, 1000);
  };

  useLayoutEffect(() => {
    reloadUseLayoutEffect();
  }, []);

  useEffect(() => {
    setUserAvatar('https://i.pravatar.cc/300');
  }, []);

  const onSend = useCallback((messages = []) => {
    setMessages(previousMessages => GiftedChat.append(previousMessages, messages));

    const { _id, createdAt, text, user } = messages[0];    
    if (order_id) {
      addDoc(collection(database, `kilapin-${order_id}`), {
        order_id,
        _id,
        createdAt,
        text,
        user
      });
    }
  }, [order_id, database]);

  return (
    <GiftedChat
      messages={messages}
      showAvatarForEveryMessage={false}
      showUserAvatar={false}
      onSend={messages => onSend(messages)}
      messagesContainerStyle={{
          backgroundColor: '#fff'
      }}
      textInputStyle={{
          backgroundColor: '#fff',
          borderRadius: 20,
      }}
      renderAvatar={(props) => {
          return <Avatar {...props} source={userAvatar} />;
      }}
      user ={{
          _id: user_id,
          order_id: order_id,
          phone: phone,
          name: name,
      }}
    />
  );
}

export default Chat;
