import React, { Component, useState, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {LinearGradient} from 'expo-linear-gradient';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Pressable, Text, StyleSheet, View, Switch } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
const colors = ['#F05941']


const Home = ({ navigation, route }) => {
  const [booking, setBooked] = useState(null);
  const [count, setCount] = useState(0);

  useEffect(() => {
    const fetchOrderBooked = async () => {
      try {
        const cleanerId = await AsyncStorage.getItem('id');
        const link = `https://cleaner.kilapin.com/order/get-order/${cleanerId}`;
        const response = await fetch(link);
        const data = await response.json();

        console.log(data.data); // Log the data received from the API

        setBooked(data.data);
      } catch (error) {
        console.error('Error fetching booked data:', error);
      }
    };

    fetchOrderBooked();
    const intervalId = setInterval(() => {
      setCount(count => count + 1);
    }, 3000);
  return () => clearInterval(intervalId);
  }, [count]);

    const handleOrder = async (orderId) => {
      try {
        console.log('menjalankan claim order',orderId)
        const reponse = await fetch(`https://cleaner.kilapin.com/order/claim-order/${orderId}`)
        const data = await reponse.json()
        console.log('claim order',data)
      } catch (error) {
        console.log(error)
      }
    }

  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);
  if (booking) {
    return (
        <View  style={styles.ContainerMain}>
                      <LinearGradient colors={['#FFFFFF', '#F5F2FF']} style={styles.BottomGradient} start={{x:0, y:0.0}} end={{x: 0, y: 0.81}}>
                </LinearGradient>
                <LinearGradient
                colors={['#5865F2', '#DD7DE1']}
                start={{ x: 0, y: 0.5 }}
                end={{ x: 1, y: 0.5 }}
                style={styles.TopBar}>
                    <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={styles.Text}>
                        Home
                    </Text>
                    <View style={{marginHorizontal: '2%'}}></View>
                    <View style={[styles.container, {marginTop: '0.5%'}]}>
          <Switch
            trackColor={{ false: '#767577', true: '#81b0ff' }}
            thumbColor={isEnabled ? '#f5dd4b' : '#f4f3f4'}
            ios_backgroundColor="#3e3e3e"
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
            </View>
        </View>
                </LinearGradient>
                <View style={styles.JobBox}>
                </View>
          <View height = '70%' flexGrow = {1} style={{ paddingBottom: hp("2%"),marginTop: hp('5%')}}>
            <ScrollView
              contentContainerStyle={{
                justifyContent: "center",
                alignContent: "center",
                alignItems: "center",
              }}
            >
              <View style>
              {/* {booked.map((booking) => ( */}
              <View key={booking._id}
               style={styles.bookingContainer}
               >
                <TouchableOpacity
                    style={{height:'100%'}}
                    onPress={() => {navigation.navigate("NavOne",{order_id: booking.order_id})}}
                >
                <Text
                style={{
                color:'#695869',
                // fontSize: 25,
                fontFamily: 'Ubuntu-Bold'
                }}
                ># {booking.order_id}</Text>
                <Text
                style={{
                color:'#fcf2fc',
                fontSize: 25,
                fontFamily: 'Ubuntu-Bold'
                  }}
                >
                {booking.address?.length > 30 ? `${booking.address.slice(0,30)}...`:booking.address}
                </Text>
                <Text
                style={{
                color:'#fcf2fc',
                fontSize: 17,
                fontFamily: 'Ubuntu-Bold'
                }}
                >
                  {booking.service_id.description}
                  </Text>
                <Text
                style={{
                color:'#8a4e8c',
                fontFamily: 'Ubuntu-Bold',
                fontSize:18
                }}
                >{booking.category}</Text>
                <Text
                style={{
                    color:'#fcf2fc',
                    fontSize: 15,
                    fontFamily: 'Ubuntu-Bold'
                    }}
                >
                  {booking.service_id.option==='Nothing' ? 'No Add-on': booking.service_id.option}
                  </Text>
                <Text
                
                >Status: {booking.status}</Text>
                </TouchableOpacity>
              </View>
            {/* ))} */}
              </View>
            </ScrollView>
          </View>
        </View>
      );
  }
  return (
    <View>
      <Text></Text>
    </View>
  )
};

const styles = StyleSheet.create({
  bookingContainer: {
    width: wp('90%'),
    backgroundColor: '#FFB6F2',
    // borderWidth: 3,
    height: hp('25%'),
    borderRadius: 35,
    padding: 20,
    marginBottom: hp('7%'),
    elevation: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },   
    container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  label: {
    marginRight: 10,
    fontSize: 16,
  },
ContainerMain: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    backgroundColor: '#fff'
},
TopBar: {
    height: 90,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
},
JobBox: {
    marginTop: 15,
},
Text: {
    color: '#FFFFFF',
    fontFamily: 'Ubuntu-Medium',
    fontSize: 24,
    fontWeight: 700,
},
BottomGradient:{
    width: '100%',
    height: '20%',
    position:'absolute',
    bottom: 0,
},
BottomMenu: {
    bottom: 0,
    position: 'absolute',
    marginBottom: 15,
},
NavOne: {
    position: 'relative',
    alignSelf: 'center'
},
Searcher: {
    
},
Prof: {
    position: 'absolute',
    top: 22,
    right: 27,
},
Msg: {
    position: 'absolute',
    top: 25,
    left: 29,
},
container: (bg) => {
    const bgcolor = colors[bg]
    return {
    width: 300,
    height: 116,
    borderRadius: 10,
    marginTop: 15,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#DA7DE1',
    borderWidth: 3
}},
CleanBox: {
    width: 56,
    height: 56,
    borderRadius: 10,
    backgroundColor: '#DA7DE1',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 14,

},
TextBox: {
    marginLeft: -6,
    width: 130,
    height: 56,
    justifyContent: 'center',

},
TaskText: {
    fontFamily: 'Ubuntu-Bold',
    fontSize: 11,
    fontWeight: '400',
    letterSpacing: 1,
},
Action: {
    top: '30%',
    marginRight:'5%',
    width: '15%',
    height: '5%',
    borderRadius: 10,
    position: 'absolute',
    right: 0,
    // margin: 14,
    backgroundColor: '#4FC76D',
    alignItems: 'center',
    justifyContent: 'center'

},
TextStyle: {
    fontFamily: 'Ubuntu-Medium',
    fontSize: 10,
    color: '#FFF',
    fontWeight: '700',
    letterSpacing: 1,
}
});

export default Home;
