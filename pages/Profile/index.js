import {
  StyleSheet,
  View,
  Text,
  Pressable,
  Image,
  ScrollView,
} from 'react-native';
import React, {useEffect,useState} from 'react';
import {LinearGradient} from 'expo-linear-gradient';

import Gap from '../../components/Gap';
import Logo from '../../assets/vector/jsx/Logo';
import Gear from '../../assets/vector/jsx/Gear';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Clean from '../../assets/vector/jsx/Clean';
const colors = ['#F05941']


const Profile = ({navigation}) => {
  
  const [name, setName] = useState('')
  const [phone, setPhone] = useState('')
  const [history, setHistory] = useState('')
  const [reloadCount, setReloadCount] = useState(0);
  const maxReloads = 2; // Reload the page 5 times

  useEffect(() => {
    const fetchMemberData = async () => {
      try {
        const name1 = await AsyncStorage.getItem('name')
        const phone1 = await AsyncStorage.getItem('phone')
        // console.log(name_localStorage)
       setName(name1)
       setPhone(phone1)

        console.log("ini name",{name1})
        // await setPhone(phone_localStorage)
      } catch (error) {
        
      }
    }

    fetchMemberData()
  },[])

  useEffect(() => {
    const fetchHistoryData = async () => {
      try {
        console.log("menjalankan get order")
        const id = await AsyncStorage.getItem('id')
        console.log(id)
        const link = `https://cleaner.kilapin.com/order/history-order/${id}`
        console.log(link)
        const response = await fetch(link)
        const data = await response.json()
        // console.log("order history",data)
        const orders = data.data
        const orders1 = orders.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
        setHistory(orders1)
        console.log("ini history",history)
      } catch (error) {
        
      }
    }

    fetchHistoryData()
  },[
  ])
  if (history) {  
  var historydone = history.filter(item => item.order_status === 'Done');
  const totalPrice = historydone.reduce((total, historydone) => total + historydone.gross_amount, 0);
  var formattedPrice = totalPrice.toLocaleString('id-ID', {style: 'currency', currency: 'IDR', minimumFractionDigits: 0});}

  return (
    <View style={styles.allcontainer}>
      <LinearGradient
        colors={['#FFFFFF', '#F5F2FF']}
        style={styles.BottomGradient}
        start={{x: 0, y: 0.0}}
        end={{x: 0, y: 0.81}}
      />
      <LinearGradient
        colors={['#5865F2', '#DD7DE1']}
        style={styles.backgroundgradient}
        start={{x: 0, y: 0.5}}
        end={{x: 1, y: 0.5}}>
        {/* <Pressable
          onPress={() =>
            navigation.navigate('Settings', {disabledAnimation: true})
          }
          style={styles.Settings}>
          <Gear />
        </Pressable> */}
        <Logo style={styles.logo} />
        <View style={styles.ppshape}>
          <View style={styles.pp} />
        </View>
      </LinearGradient>
      
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Gap height={80} />
        <Text style={styles.profilename}>{name}</Text>
        <Gap height={9} />
        <View style={{flexDirection:'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center'}}>
        <Text style={styles.phonenumber}>{phone}</Text>
        <Gap width={10}/>
        <Text style={styles.phonenumber} onPress={() => navigation.navigate('NavOne')}>-  {formattedPrice}</Text>
        </View>
        <Gap height={40} />
        <Text style={styles.history}>Order History</Text>
      </View>
      <Gap height={20} />
      <View height = '45%' flexGrow = {1} style={{ paddingBottom: "28%",}}>
      <ScrollView
          contentContainerStyle={{
            justifyContent: "center",
            alignContent: "center",
            alignItems: "center",
          }}
        >
        <View style={{marginTop: '-5%'}}>
        {history ? (
                    <View>{history.map((urgent) => {
                        return (
                            <View style={styles.container()}
                            key = {urgent.order_id}>
                            {(urgent.order_status === "Done") ? (
                            <View>
                            <Pressable>
                            <View style={[styles.CleanBox,{backgroundColor: '#4FC76D'}]}
                            key = {urgent.order_id}>
                            <Clean/>
                            </View>
                            <View style={styles.TextBox}></View>
                            <View style={{justifyContent: 'center', alignItems: 'center', marginTop: '-17%', marginBottom: '10%', marginLeft: '10%', }}>
                            <Text style={[styles.TaskText]}>{urgent.item_name}</Text>
                            </View>
                            <View style={{marginLeft: '75%', marginBottom: '27.5%', marginTop: '-27%'}}>
                            <Text style={{fontSize: 10,}}>{urgent.order_id}</Text>
                            <Text style={{fontSize: 10,}}>{urgent.address.substring(0, 20)}...</Text>
                            <Text style={{fontSize: 10,}}>{urgent.order_status}</Text>
                            <Text style={{fontSize: 10,}}>{urgent.gross_amount}</Text>
                            </View>
                            </Pressable>
                            </View>
                            ) : ((urgent.order_status === "Cancel") ? (
                              <View>
                              <View style={[styles.CleanBox,{backgroundColor: '#F05941'}]}
                              key = {urgent.order_id}>
                              <Clean/>
                              </View>
                              <View style={styles.TextBox}></View>
                              <View style={{justifyContent: 'center', alignItems: 'center', marginTop: '-17%', marginBottom: '10%', marginLeft: '10%', }}>
                              <Text style={[styles.TaskText]}>{urgent.item_name}</Text>
                              </View>
                              <View style={{marginLeft: '75%', marginBottom: '27.5%', marginTop: '-27%'}}>
                              <Text style={{fontSize: 10,}}>{urgent.order_id}</Text>
                              <Text style={{fontSize: 10,}}>{urgent.address.substring(0, 20)}...</Text>
                              <Text style={{fontSize: 10,}}>{urgent.order_status}</Text>
                              <Text style={{fontSize: 10,}}>{urgent.gross_amount}</Text>
                              </View>
                              </View>
                            ) : (
                              <View>
                              <Pressable onPress={
                              () => navigation.navigate("NavOne",{order_id: urgent.order_id})}>
                              <View style={[styles.CleanBox]}
                              key = {urgent.order_id}>
                              <Clean/>
                              </View>
                              <View style={styles.TextBox}></View>
                              <View style={{justifyContent: 'center', alignItems: 'center', marginTop: '-16.8%', marginBottom: '10%', marginLeft: '10%'}}>
                              <Text style={[styles.TaskText]}>{urgent.item_name}</Text>
                              </View>
                              <View style={{marginLeft: '75%', marginBottom: '27.5%', marginTop: '-27%'}}>
                              <Text style={{fontSize: 10,}}>{urgent.order_id}</Text>
                              <Text style={{fontSize: 10,}}>{urgent.address.substring(0, 20)}...</Text>
                              <Text style={{fontSize: 10,}}>{urgent.order_status}</Text>
                              <Text style={{fontSize: 10,}}>{urgent.gross_amount}</Text>
                              </View>
                              </Pressable>
                              </View>
                            )
                            )}
                          </View>
                        )
                    })}</View>
                ) : (
                    <Text>Tidak Ada History</Text>
                )}
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  allcontainer: {
    width: '100%',
    height: '100%',
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  Settings: {
    position: 'absolute',
    top: 24,
    marginTop: '12%',
    right: 23,
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  backgroundgradient: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    width: '100%'
  },
  logo: {
    marginTop: '30%',
    marginBottom: 30,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  ppshape: {
    backgroundColor: '#000',
    padding: 5,
    marginBottom: -70,
    borderRadius: 100,
    backgroundColor: '#F2F2F2',
  },
  pp: {
    backgroundColor: '#C3C3C3',
    padding: 60,
    borderRadius: 100,
  },
  profilename: {
    fontFamily: 'Ubuntu-Bold',
    fontSize: 24,
    fontWeight: '500',
    letterSpacing: 1,
    color: '#1E2022',
  },
  phonenumber: {
    fontFamily: 'Ubuntu-Medium',
    fontSize: 18,
    lineHeight: 18,
    letterSpacing: 1,
    color: '#77838F',
  },
  history: {
    color: '#1E2022',
    fontStyle: 'normal',
    fontFamily: 'Ubuntu-Bold',
    fontWeight: '500',
    letterSpacing: 1,
    lineHeight: 21,
    fontSize: 20,
    alignSelf: 'center',
  },
  data: {
    fontFamily: 'Ubuntu-Medium',
    fontSize: 14,
    color: '#1E2022',
    letterSpacing: 1,
    lineHeight: 21,
  },
  BottomGradient: {
    width: '100%',
    height: '20%',
    position: 'absolute',
    bottom: 0,
  },
  JobBox: {
    margin: 20,
    alignItems: 'center',
  },
  BottomMenu: {
    bottom: 0,
    position: 'absolute',
    marginBottom: 15,
    alignSelf: 'center',
  },ContainerMain: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    backgroundColor: '#FFFFFF'
},
TopBar: {
    height: 90,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
},
JobBox: {
    marginTop: 15,
},
Text: {
    color: '#FFFFFF',
    fontFamily: 'Ubuntu-Medium',
    fontSize: 24,
    fontWeight: 700,
},
BottomGradient:{
    width: '100%',
    height: '20%',
    position:'absolute',
    bottom: 0,
},
BottomMenu: {
    bottom: 0,
    position: 'absolute',
    marginBottom: 15,
},
NavOne: {
    position: 'relative',
    alignSelf: 'center'
},
Searcher: {
    
},
Prof: {
    position: 'absolute',
    top: 22,
    right: 27,
},
Msg: {
    position: 'absolute',
    top: 25,
    left: 29,
},
container: (bg) => {
    const bgcolor = colors[bg]
    return {
    width: 300,
    height: 116,
    borderRadius: 10,
    marginTop: 15,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#DA7DE1',
    borderWidth: 3
}},
CleanBox: {
    width: 56,
    height: 56,
    borderRadius: 10,
    backgroundColor: '#DA7DE1',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '5%',
    marginTop: '27%'

},
TextBox: {
    marginLeft: '50%',
    width: 120,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center'
},
TaskText: {
    fontFamily: 'Ubuntu-Bold',
    fontSize: 11,
    fontWeight: '400',
    letterSpacing: 1,
    flexDirection: 'row',
    width: '45%',
    justifyContent: 'center',
    alignItems: 'center'
},
Action: {
    width: 63,
    height: 63,
    borderRadius: 10,
    position: 'absolute',
    right: 0,
    margin: 14,
    backgroundColor: '#4FC76D',
    alignItems: 'center',
    justifyContent: 'center'

},
TextStyle: {
    fontFamily: 'Ubuntu-Medium',
    fontSize: 14,
    color: '#FFF',
    fontWeight: '700',
    letterSpacing: 1,
}
});

export default Profile;
